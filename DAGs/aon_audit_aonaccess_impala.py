from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_audit_aonaccess_impala_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_audit_aonaccess_impala',schedule_interval='0 11 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Latam']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
    #Preparing environment [only valid in snowflake migration]
    Preparation = executingjob(dag,'81aa9e9c-6be2-44fd-8138-018b3178fb4d:aon.com','Preparation')
    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
    #Finishing process
    Close = CloseTask(dag,jobs_close)
#Managing dependencies
connect>>Preparation>>Taks>>Close

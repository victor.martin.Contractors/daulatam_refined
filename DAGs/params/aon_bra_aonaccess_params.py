ThreadQuantity = 3
jobs = [ 
        [0,['sg_siniestroestado',['raw','25176951-0ba7-45df-b427-7b42ee2b6697:aon.com'],['discovery','8d5eddb3-b357-46b7-a9f8-e3708c8a5da9:aon.com']]],
        [1,['sg_siniestroestadoaseguradora',['raw','3e474c6d-4e54-4393-8755-b2b9f901ea07:aon.com'],['discovery','04451677-b5f0-40e9-ab19-7f29229450ef:aon.com']]],
        [2,['sg_vendedorgrupo',['raw','8a2406e3-007e-49f1-9c41-45a76573df6a:aon.com'],['discovery','5970ca45-78f0-4d11-a5cf-8a13e1439161:aon.com']]],
        [0,['tg_tipodocumento',['raw','1bfb1ebe-4e81-48c2-8a77-1516185c0526:aon.com'],['discovery','3ca330a1-74d9-4794-b988-6001bf864f3b:aon.com']]], 
        [1,['tg_moneda',['raw','b2726ff1-20bd-4558-824a-1aaf0086091f:aon.com'],['discovery','7617874d-3a8d-40d8-a3f7-e492275ba41d:aon.com']]],
        [2,['sg_siniestrotipocierre',['raw','dcff9d8c-2f4a-48ef-8ebd-7f73b3834f5d:aon.com'],['discovery','f6e6dd53-b86a-466d-bf3f-45d2827d3800:aon.com']]],
        [0,['sg_ramo',['raw','7bf0971d-9f7a-49d7-b06e-d09c7619b0e4:aon.com'],['discovery','1971fbd0-2163-4a8e-8752-a19ad32dd5ed:aon.com']]],
        [1,['tg_pais',['raw','4b6ebb7e-cb4e-43d6-a1be-f33624af93bf:aon.com'],['discovery','43600acc-b66d-4d8a-8cf2-e8c1bfef51f2:aon.com']]], 
        [2,['sg_seccion',['raw','7c08bc11-7168-4990-b77f-d3a555a230ae:aon.com'],['discovery','4609ea41-f924-4c87-a0bb-45f3f6f014b4:aon.com']]],
        [0,['sg_siniestroliquidador',['raw','e6ece326-7495-4837-a56d-3ab6dc8235ad:aon.com'],['discovery','6a9eea00-d4aa-41f8-a175-de6b0ce1678b:aon.com']]],
        [1,['sg_parametro',['raw','d512d32b-9f7b-4dc0-98a0-21ec12e2e20a:aon.com'],['discovery','7b820cb8-d26b-4971-ab84-eba125638586:aon.com']]],
        [2,['sg_siniestrotipocausa',['raw','a7ab5d19-f9eb-4e17-9671-e2d296b42735:aon.com'],['discovery','e43536b8-020b-4d20-9209-a8c60cdf0220:aon.com']]], 
        [0,['sg_subactividad',['raw','9828cf40-4ec4-4323-8f60-823b7aaa390c:aon.com'],['discovery','3052f3f9-3ad8-4519-b275-b60df538ef37:aon.com']]],
        [1,['sg_actividad',['raw','7ee5295c-f592-4b04-aba1-aef96d40df12:aon.com'],['discovery','6a1f37dc-21c9-4a7a-8a1c-bdea0d8e8297:aon.com']]],
        [2,['tg_monedacotizacion',['raw','a273d17d-32ea-43a3-a622-8ee883aa01d0:aon.com'],['discovery','a076def8-8d0f-44f4-9d79-99f3c70a9e71:aon.com']]],
        [0,['sg_segmentacioncartera',['raw','4e5f562b-b56f-4b31-b798-c112577da7a2:aon.com'],['discovery','9e08f475-9be4-4ea9-ad35-414af7eca1c1:aon.com']]], 
        [1,['sg_cobertura',['raw','d98628b7-2560-4b48-bb64-2b0a71d3753a:aon.com'],['discovery','e594ea69-8a95-4e01-88e0-9b6dfce36692:aon.com']]],
        [2,['sg_siniestrocausa',['raw','fa51347d-6696-4492-a0db-df09a6f67827:aon.com'],['discovery','46c7adbd-cade-4afa-ad39-478c00fe5e34:aon.com']]],
        [0,['u_sg_cliente',['raw','36ba73ef-b429-439d-9e32-f868dd11b6db:aon.com'],['discovery','39681cf2-b72b-4239-a977-e8b65ccbeb65:aon.com']]],
        [1,['sg_cliente',['raw','ba193fdc-f888-49e1-81c9-8841994c2536:aon.com'],['discovery','decd74aa-632b-479b-9b54-f82c5229ac95:aon.com']]], 
        [2,['sg_saldosiniestro',['raw','cc713591-e520-4e55-989f-767f8af09fa7:aon.com'],['discovery','e9d09d28-38ed-40d4-9571-2a52b22a6d62:aon.com']]],
        [0,['sg_siniestros',['raw','e05071ca-a149-40c1-8671-d98f7f380dc7:aon.com'],['discovery','60bc0ef9-8eb3-4fed-a6ee-6b4b3a1c0135:aon.com']]],
        [1,['sg_siniestroreclamo',['raw','8a5d30dc-81a8-4b9a-90b5-457c145b955e:aon.com'],['discovery','3c0edf67-1362-4d28-b7b0-5d77e6fa6442:aon.com']]],
        [2,['sg_siniestrocuentacorriente',['raw','07ec7e5e-8b5e-4196-91c7-eab470cd00aa:aon.com'],['discovery','1b7043d9-20d6-414f-9bd6-3284b2d7d715:aon.com']]], 
        [0,['tg_ente',['raw','055c308f-dd68-4875-b2ad-a582aabef525:aon.com'],['discovery','82b29f5f-e5e7-4152-811f-308864a2f53e:aon.com']]],
        [1,['sg_siniestrodocumentacion',['raw','d2cf2a22-14d5-42cd-81c9-f4767f59fc7d:aon.com'],['discovery','44d1d697-6586-481f-bbb6-c1517da18ada:aon.com']]],
        [2,['sg_operacion',['raw','ef06b066-1d03-46b1-8c15-16e9f88f84b7:aon.com'],['discovery','637a182c-b17e-4700-ac17-1bb94b4fb196:aon.com']]],
        [0,['sg_codigoindustrial',['raw','3832452c-5e2c-4337-ae47-2e7fdd744d64:aon.com'],['discovery','cc41e480-5a0a-472d-b353-144c902c9dad:aon.com']]], 
        [1,['u_sg_operacion',['raw','0793f2ba-b34f-49be-9792-31fcff48464a:aon.com'],['discovery','4ddfa889-935a-43f7-a6c5-839dcd4b3b73:aon.com']]],
        [2,['ut_cvtablavalor',['raw','bd98afa0-38c5-4223-a0d3-4eabddd70570:aon.com'],['discovery','0bb124c7-bd0d-45eb-88e7-fa05fbdd2952:aon.com']]],
        [0,['u_sg_grupoeconomico',['raw','6d96022c-2982-425c-97b0-8b5dbfd1bb58:aon.com'],['discovery','8bb2d7a8-524e-4d29-9db5-2e21bac0915f:aon.com']]],
        [1,['u_tg_pais',['raw','79a2b8ff-710c-4313-9399-c175ffa51166:aon.com'],['discovery','eabb1fbb-447a-44b4-b67f-0cd6d54cd639:aon.com']]],
        [1,['u_tg_moneda',['raw','646e645f-1cc7-4e63-9289-7afd584e1e1f:aon.com'],['discovery','ca2b70a6-05e5-405f-b419-9d8133192f06:aon.com']]],
        [2,['u_sg_seccion',['raw','44499417-9aae-4e69-8f28-1a2b015b92d7:aon.com'],['discovery','ed2c42cd-485f-4ae0-acde-b6701101161b:aon.com']]],
        [3,['tg_provincia',['raw','4bad2354-819e-4805-a0ee-0094314a1e14:aon.com'],['discovery','dcb0a362-9077-4e4e-b668-3b7900b438ab:aon.com']]],
        [0,['sg_grupoindustrial',['raw','de398c15-dafd-4f6f-b467-48aa615aee55:aon.com'],['discovery','fdfef6bc-67ba-4167-8922-ad241abb81f5:aon.com']]],
        [1,['sg_aseguradora',['raw','9d1da3cd-dbd3-43dd-9725-9cd477cd11e4:aon.com'],['discovery','435f620d-c4fc-43ee-b795-ced2eb2f628a:aon.com']]],
        [1,['sg_clasificacioncuenta',['raw','4b196951-fa2f-4353-860c-6e25855048fb:aon.com'],['discovery','19588537-bd9d-4f76-899a-f6928c497d91:aon.com']]],
        ]

jobs_close = [ 
        ['Brazil_impala','24e97c0a-ca0a-4675-a16d-b53c901923c6:aon.com'], 
        ['Brazil','5a9d665f-fe58-4290-ad5b-3f39ae9c62d2:aon.com'],
] 

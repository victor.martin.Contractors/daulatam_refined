ThreadQuantity = 4
jobs = [ 
        [0,['Argentina_sg_actividad',['raw','f32dd661-150c-4920-9721-21394c9a3ab9:aon.com'],['discovery','660287c7-1f26-4c35-a12d-c7a6590946e1:aon.com']]],
        [0,['Sigb_ad_associacao',['raw','43f6d420-748b-4558-9bcf-9e79d6879202:aon.com'],['discovery','03b04677-443d-4f87-9255-a88510fd06e4:aon.com']]],
        [0,['Chile_sg_aseguradoraconsulta',['raw','b3905469-7546-4b87-b33e-286b48695cde:aon.com'],['discovery','903de9e9-dabf-4104-89b9-43b7d1cd3d26:aon.com']]],
        [1,['Chile_sg_contratohonorarioconsulta',['raw','bb33cb20-0646-452d-b738-b6830e124a42:aon.com'],['discovery','0d20c1f3-6847-4ab7-a686-ddcc5cbda1bf:aon.com']]],
        [1,['Chile_sg_honorarioitem',['raw','e6063b3d-23ff-466a-adbc-b4f6fa8b8c73:aon.com'],['discovery','10e5b919-3e80-4192-be30-78af25cf157e:aon.com']]],
        [1,['Venezuela_sg_parametro',['raw','1d4124cd-45c3-4cc5-a4e8-b537548aa6be:aon.com'],['discovery','8089fb63-4e9b-4975-88ac-4131c8a97902:aon.com']]],
        [2,['Argentina_sg_siniestrocausa',['raw','b78d273b-20e1-41fa-94fa-fc5f8b5dcbbf:aon.com'],['discovery','31d3c1e9-3e34-437e-9d87-49394b5acca2:aon.com']]],
        [2,['Colombia_mnp_segmentacion',['raw','08759427-0b1d-4a1f-ab73-26c6aaf89179:aon.com'],['discovery','57aeac2e-fac6-4ec4-80a5-a4ffd02c6f3c:aon.com']]],
        [2,['Colombia_mv_arsco_vista_21',['raw','1fcda182-e0f8-406e-b8d4-3ab63cebe16f:aon.com'],['discovery','210c2595-1bc0-4492-a322-abe1864695b0:aon.com']]],
        [3,['Argentina_sg_honorarioitem',['raw','9a0465fa-91e5-4ac6-9c53-bdcc577973e2:aon.com'],['discovery','9bcf8726-9e76-4d4a-9587-dc1ba5c31bdc:aon.com']]],
        [3,['Argentina_tg_entedomicilio',['raw','221192f1-7e9f-47ee-acf6-62492cd09635:aon.com'],['discovery','dcd19a94-0757-46ab-8b4b-103d5a531a0b:aon.com']]],
        [3,['Chile_sg_operacion',['raw','caf31b4c-6a5e-4bcb-85e9-7c8fe6b5c0d4:aon.com'],['discovery','0c7608bf-826b-4468-958e-bcf001f0221a:aon.com']]],
        [1,['Colombia_base_ppto',['raw','99b4d4a6-cccf-4aff-849e-99ce97ea2cbd:aon.com'],['discovery','fa5ca925-deb1-4b7b-8a87-b8620cd5df53:aon.com']]],
        [0,['Peru_sg_siniestrotipocierre',['raw','6bea369c-879f-485d-b3c9-1bb657753ef9:aon.com'],['discovery','f825faab-50b7-4166-98f0-26c9d12af5fa:aon.com']]],
]

jobs_close = [ 
        ['Argentina_impala','1f1ee612-9235-467a-b0c3-aca9669c51ea:aon.com'], 
        ['Argentina','351851ee-dd17-46c6-93b2-b15f13860fb1:aon.com'],
]

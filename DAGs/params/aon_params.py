import os
from src.model.Jobexecution import *
from src.controller.Taskcontroller import *
from src.model.Taskgroup import *
from datetime import datetime
from airflow.models import XCom

#AWS Secret Manager
default_secret_region = "eu-west-1"
secret_sdk_activation_name = "dev/daulatam/airflow/streamsets-sdk-activation-key"
default_secret_service_name = "secretsmanager"

#Streamsets connection properties
STREAMSETSCONTROLHUBURL = 'https://cloud.streamsets.com'
STREAMSETSSTAGEWAITINGTIME = 32000

#Variables
Threads = []
Threads_close = []
Thread_cicle =0
#Args
default_args = {'start_date': datetime(2020,1,1) }

#Proxy properties
proxyHost = "serverproxy.aon.net"
proxyPort = "8888"
os.environ['http_proxy']='http://{}:{}'.format(proxyHost, proxyPort)
os.environ['https_proxy']='http://{}:{}'.format(proxyHost, proxyPort)

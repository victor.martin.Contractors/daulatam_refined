from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_chl_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_chl_aonaccess_flow',schedule_interval='0 1 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Chile']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
    #Preparing environment [only valid in snowflake migration]
    #truncate = executingjob(dag,'51c5cdb1-b6f3-459e-a5fb-dc12c72a3069:aon.com','snowflaketruncate')
    #sg_operacionconsulta_clean=executingjob(dag,'e1ae562-b346-4ea4-b52a-1486a2a13e82:aon.com','sg_operacionconsulta_clean')
    #sg_operacionconsulta_raw=executingjob(dag,'26a1ec2f-5003-4328-88ad-c67d4aedaf65:aon.com','sg_operacionconsulta_raw')
    #sg_operacionconsulta_discovery=executingjob(dag,'1bb05a71-0fec-4f13-8f42-a13691729977:aon.com','sg_operacionconsulta_discovery')
    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
    #Finishing process
    #Close = CloseTask(dag,jobs_close)   
    #sg_operacionconsulta_clean>>sg_operacionconsulta_raw>>sg_operacionconsulta_discovery
#Managing dependencies  
connect>>[Taks]
from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_col_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_col_aonaccess_flow',schedule_interval='0 2 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Colombia']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
    #Preparing environment [only valid in snowflake migration]
    mv_arsco_vista_21_clean = executingjob(dag,'f2023c41-ccd1-4692-b32c-5a591f23a154:aon.com','mv_arsco_vista_21_clean')
    mv_arsco_vista_21_raw = executingjob(dag,'3cdca017-9715-4b69-8217-4e40e396ecd9:aon.com','mv_arsco_vista_21_raw')
    mv_arsco_vista_21_discovery = executingjob(dag,'210c2595-1bc0-4492-a322-abe1864695b0:aon.com','mv_arsco_vista_21_discovery')

    base_ppto_clean = executingjob(dag,'0e604299-3e81-403a-a55d-9ac8fa5daae5:aon.com','base_ppto_clean')
    base_ppto_raw = executingjob(dag,'99b4d4a6-cccf-4aff-849e-99ce97ea2cbd:aon.com','base_ppto_raw')
    base_ppto_discovery = executingjob(dag,'fa5ca925-deb1-4b7b-8a87-b8620cd5df53:aon.com','base_ppto_discovery')

    sg_saldoporcuota_clean = executingjob(dag,'e1f9a43d-bbaf-4b05-ba85-9839959bb313:aon.com','sg_saldoporcuota_clean')
    sg_saldoporcuota_raw = executingjob(dag,'b45e9b6e-f4cb-46b5-9877-12c626866266:aon.com','sg_saldoporcuota_raw')
    sg_saldoporcuota_discovery = executingjob(dag,'74dd375c-1eb3-4466-90f3-2804ae825056:aon.com','sg_saldoporcuota_discovery')

    sg_saldoporcuota_clean>>sg_saldoporcuota_raw>>sg_saldoporcuota_discovery
    base_ppto_clean>>base_ppto_raw>>[base_ppto_discovery,sg_saldoporcuota_clean]
    mv_arsco_vista_21_clean>>[mv_arsco_vista_21_raw,base_ppto_clean]>>mv_arsco_vista_21_discovery
    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
    #sg_saldoporcuota_raw>>sg_saldoporcuota_discovery
    #Finishing process
    #Close = CloseTask(dag,jobs_close) 
#Managing dependencies  
connect>>[Taks,mv_arsco_vista_21_clean]
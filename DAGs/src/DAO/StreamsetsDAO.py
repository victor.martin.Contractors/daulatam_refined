import boto3
import base64
import logging
from params.aon_params import *
from botocore.exceptions import ClientError
# [Streamsets Activation key]
def get_activation_key():
    secret_name = "dev/daulatam/airflow/streamsets-sdk-activation-key"
    #secret_name = "prod/daulatam/Streamsets/KeySDK29012023"
    #              "prod/daulatam/Streamsets/KeySDK29012023"
    region_name = "eu-west-1"
    
    secret = ""
    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name="secretsmanager",
        region_name=region_name
    )
    
    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )

    except ClientError as e:
        logging.info('Client connection Error -> '+e.response['Error']['Code'])
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            logging.info('Secrets Manager can not decrypt the protected secret text using the provided KMS key.')
            raise e

        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            logging.info('An error occurred on the server side.')
            raise e

        elif e.response['Error']['Code'] == 'InvalidParameterException':
            logging.info('You provided an invalid value for a parameter.')
            raise e

        elif e.response['Error']['Code'] == 'InvalidRequestException':
            logging.info('You provided a parameter value that is not valid for the current state of the resource.')
            raise e

        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            logging.info('We can not find the resource that you asked for.')
            raise e
    else:
        # Decrypts secret using the associated KMS key.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
        else:
            secret = base64.b64decode(get_secret_value_response['SecretBinary'])
    return secret.replace('"', '')

# User and Password
def get_streamsets_user():
    #secret_name = "dev/daulatam/airflow/streamsets-user"
    #get_secret_value_response = client.get_secret_value(
    #    SecretId=secret_name
    #)
    user = 'svc-ARNH137639@aon.com'
    return user

def get_streamsets_password():
    #secret_name = "dev/daulatam/airflow/streamsets-password"
    #get_secret_value_response = client.get_secret_value(
    #    SecretId=secret_name
    #)
    Password = 'ZiP?A0K3fi6rx%kUMX46'
    return Password

from streamsets.sdk import ControlHub
from src.DAO.StreamsetsDAO import *
import threading

class SingletonControlHub:
    _instance_lock = threading.Lock()
    _control_hub = None
    _job_list = None

    def __new__(cls, *args, **kwargs):
        logging.info('-->1')
        if not hasattr(cls, "_instance"):
            logging.info('-->2')
            with cls._instance_lock:
                logging.info('-->3')
                if not hasattr(cls, "_instance"):
                    logging.info('-->4')
                    cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, control_hub_url, username, password):
        if SingletonControlHub._control_hub is None:
            logging.info('singleton Control hub Loggin')
            SingletonControlHub._control_hub = ControlHub(server_url=control_hub_url,
                                                          username=username,
                                                          password=password)
            SingletonControlHub._job_list = SingletonControlHub._control_hub.jobs.get_all()

    @classmethod
    def get_instance(cls):
        logging.info('singleton get instance')
        if cls._instance is None:
            logging.info('singleton getting instance')
            control_hub_url='https://cloud.streamsets.com'
            username=get_streamsets_user()
            password=get_streamsets_password()

            cls(control_hub_url, username, password)
        return cls._instance

    @classmethod
    def get_job_list(cls):
        return cls._job_list

    
    @classmethod
    def get_control_hub_conn(cls):
        logging.info('checking singleton')
        if cls._control_hub is None:
            logging.info('singleton getting instance')
            control_hub_url='https://cloud.streamsets.com'
            username=get_streamsets_user()
            password=get_streamsets_password()

            cls(control_hub_url, username, password)

        return cls._control_hub
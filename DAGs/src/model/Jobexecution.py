from streamsets.sdk import ControlHub
import streamsets.sdk as sdk
from params.aon_params import *
from src.DAO.StreamsetsDAO import *
from src.model.SingletonControlHub import *
from time import sleep
import logging
import os
import json
import pickle
from airflow.models import XCom

# [Streamsets connection]
#os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = ''

def getting_jobs(**kwargs):
    logging.info('Control hub Loggin')
    #control_hub = control_hub_exec()         
    control_hub = SingletonControlHub.get_control_hub_conn()
    logging.info('Control hub Loggin ###2###')
    control_hub = SingletonControlHub.get_control_hub_conn()

    #logging.info('get_all_jobs')
    #all_jobs = get_jobs_all(control_hub)
#it works
    #logging.info('get_job')
    #job=all_jobs.get(job_id='f32dd661-150c-4920-9721-21394c9a3ab9:aon.com')
    #logging.info('end_get_job')

    #logging.info('running_job')
    #control_hub.start_job(job)
    #logging.info('waiting for job completion')
    #control_hub.wait_for_job_status(job,'INACTIVE', timeout_sec=18000)
    #logging.info('end_running_job')
    return 'connecting'

def control_hub_exec(**context):
    streamsetsUser = get_streamsets_user()
    streamsetsPassword = get_streamsets_password()
    logging.info('Control hub Loggin')
    control_hub = ControlHub(server_url='https://cloud.streamsets.com',
                             username=streamsetsUser,
                             password=streamsetsPassword)
    context['ti'].xcom_push(key='snowflake_conn',value=control_hub)

    return control_hub


def get_jobs_without(**context):
    control_hub = context['ti'].xcom_pull(key='snowflake_conn')
    all_jobs = control_hub.jobs.get_all()
    return all_jobs


def get_jobs_all(control_hub):
    all_jobs = control_hub.jobs.get_all()
    return all_jobs

def executing_job_from_list(job_identifier,job_list):
    logging.info('executing_job_from_list')
    streamsetsUser = get_streamsets_user()
    streamsetsPassword = get_streamsets_password()
   # control_hub = SingletonControlHub.get_control_hub_conn()        
    control_hub = ControlHub(server_url='https://cloud.streamsets.com',
                             username=streamsetsUser,
                             password=streamsetsPassword)

    logging.info('get_all_jobs')
    all_jobs = get_jobs_all(control_hub)

    job_identifier = 'c9f85d4c-aaf5-4b7c-af48-a43c42a3ed49:aon.com'
    job=all_jobs.get(job_id=job_identifier)
    
    logging.info('updating params')
    job.runtime_parameters['tenant'] = 'daulatam/0fc7411b-fd52-4d6a-92b1-fe6659357da5/prod/SG_OPERACION_testing/inc/'
    print(f'Parameter2: {job.runtime_parameters}')
       
    logging.info('Executing job')
    control_hub.start_job(job)
    logging.info('waiting for job completion')
    control_hub.wait_for_job_status(job,'INACTIVE', timeout_sec=18000)
    logging.info('Finished')
    return f'job_executed_{job_identifier}'




def connecting_to_streamsets(job_identifier):
    logging.info('Getting user and password')
    streamsetsUser = get_streamsets_user()
    streamsetsPassword = get_streamsets_password()
    logging.info('Control hub Loggin')
    control_hub = ControlHub(server_url='https://cloud.streamsets.com',
                             username=streamsetsUser,
                             password=streamsetsPassword)
    logging.info('Getting job Status')
    #all_jobs = control_hub.jobs.get_all()    
    #globals()['all_jobs'] = control_hub.jobs.get_all()
    #Variable.set("all_jobs",all_jobs)
    
    
    #logging.info('-------')
    #all_jobs_read = Variable.get("all_jobs",deserialize_json=False);

    #for myjob in all_jobs_read:
    #     logging.info(myjob.job_id)


    #logging.info('---executing----')
    
    #myjob=all_jobs_read.get('b78d273b-20e1-41fa-94fa-fc5f8b5dcbbf:aon.com')
    #logging.info('---get executing----')
    
    #logging.info('Executing my job Status')
    #control_hub.start_job(myjob)
    logging.info('Finish')
    return 'connecting'

# [Job Execution]
def executing_job(job_identifier):
    try:
        logging.info('Getting user and password')
        streamsetsUser = get_streamsets_user()
        streamsetsPassword = get_streamsets_password()
        logging.info('Control hub Loggin')
        control_hub = ControlHub(server_url='https://cloud.streamsets.com',
                                username=streamsetsUser,
                                password=streamsetsPassword)
        #logging.info('start try')
        #myjobs=globals()['all_jobs']
        #myjob=all_jobs.get('b78d273b-20e1-41fa-94fa-fc5f8b5dcbbf:aon.com')
        #logging.info('executing try')
        #control_hub.start_job(myjob)
        #logging.info('end try')
        logging.info('Getting job Status')
        job = control_hub.jobs.get(job_id=job_identifier)
        #control_hub.wait_for_job_status(job,'INACTIVE', timeout_sec=200)
        logging.info('Executing job')
        control_hub.start_job(job)
        logging.info('waiting for job completion')
        control_hub.wait_for_job_status(job,'INACTIVE', timeout_sec=18000)
        logging.info('Finished')
    except:
        sleep(120)  # Time in seconds
        logging.info('Retrying Streamsets execution')
        logging.info('Getting user and password')
        streamsetsUser = get_streamsets_user()
        streamsetsPassword = get_streamsets_password()
        logging.info('Control hub Loggin')
        control_hub = ControlHub(server_url='https://cloud.streamsets.com',
                                username=streamsetsUser,
                                password=streamsetsPassword)
        logging.info('Getting job Status')
        job = control_hub.jobs.get(job_id=job_identifier)
        #control_hub.wait_for_job_status(job,'INACTIVE', timeout_sec=200)
        logging.info('Executing job')
        control_hub.start_job(job)
        logging.info('waiting for job completion')
        control_hub.wait_for_job_status(job,'INACTIVE', timeout_sec=18000)
    return f'job_executed_{job_identifier}'

def get_data_collector_status():
    logging.info('Getting user and password')
    streamsetsUser = get_streamsets_user()
    streamsetsPassword = get_streamsets_password()
    logging.info('Control hub Loggin')
    control_hub = ControlHub(server_url='https://cloud.streamsets.com',
                               username=streamsetsUser,
                               password=streamsetsPassword)
    
    logging.info('Gettin Data collectors')
    data_collectors = control_hub.data_collectors.list()
    logging.info('Get')
    for dc in data_collectors:
        logging.info('Get name ')
        print(dc.name)
        logging.info('Data collector: '+dc.name)

def get_job_stats():
    client = sdk.DataCollectorClient('https://1.sdc.eap.aon.com:18630')
    data_collectors = client.list_data_collectors()
    for data_collector in data_collectors:
        print(f"Data Collector {data_collector['id']}:")
        jobs = client.list_jobs(data_collector['id'])
        for job in jobs:
            stats = client.get_job_stats(data_collector['id'], job['jobId'])
            print(f"\tJob {job['jobId']}: Memory - {stats['memory']}, CPU - {stats['cpu']}")
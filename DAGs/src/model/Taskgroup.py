from airflow.utils.task_group import TaskGroup
from airflow.operators.python_operator import PythonOperator
from src.model.Jobexecution import *

#[First connection and environment comprobation]
def connectToStreamsets(dag):
    connect = PythonOperator(
                            task_id='get_data_collector_status',
                            provide_context=True,
                            python_callable=connecting_to_streamsets,
                            op_kwargs={'job_identifier': ''},
                            dag=dag,
    )
    return connect

def gettingcontrolhub(dag):
    hub = PythonOperator(
                            task_id='control_hub_exec',
                            provide_context=True,
                            python_callable=control_hub_exec,
                            dag=dag,
    )
    return hub
    

def gettingjobsStreamsets(dag):
    connect = PythonOperator(
                            task_id='getting_jobs',
                            provide_context=True,
                            python_callable=getting_jobs,
                            dag=dag,
    )
    return connect



#[Punctual job executor]
def executingjobfromlist(dag,jobid,taskname):
    connect = PythonOperator(
                            task_id=taskname,
                            provide_context=True,
                            python_callable=executing_job_from_list,
                            op_kwargs={'job_identifier': jobid,'job_list':"{{ ti.xcom_pull(key='job_list')}}" },
                            dag=dag,
    )
    return connect



#[Punctual job executor]
def executingjob(dag,jobid,taskname):
    connect = PythonOperator(
                            task_id=taskname,
                            provide_context=True,
                            python_callable=executing_job,
                            op_kwargs={'job_identifier': jobid},
                            dag=dag,
    )
    return connect

#[Thask thread constructor,group taks by thread with dependences]
def createTaskGroup(jobs,Thread_cicle,dag,paths):
    jobsgGroup1 = 0
    jobsgGroup2 = 1
    jobsgGroup3 = 2
    groupsThread = []
    with TaskGroup(group_id=f'Thread_{Thread_cicle}',parent_group=paths) as Thread:
        i = 0
        processQuantity = 0
        while i < len(jobs):
            if jobs[i][0] ==Thread_cicle:
                raw =PythonOperator(
                    task_id=f'{jobs[i][1][0]}_{jobs[i][1][1][0]}',
                    trigger_rule='all_done', 
                    python_callable=executing_job,
                    op_kwargs={'job_identifier': f'{jobs[i][1][1][1]}'},
                    dag=dag,
                )
                discovery =PythonOperator(
                    task_id=f'{jobs[i][1][0]}_{jobs[i][1][2][0]}',
                    python_callable=executing_job,
                    op_kwargs={'job_identifier': f'{jobs[i][1][2][1]}'},
                    dag=dag,
                )
                groupsThread.append(raw)
                groupsThread.append(discovery)
                processQuantity+= 2
            i += 1
        groupsThread[0]>>[groupsThread[1],groupsThread[2]]
        processQuantity-= 3
        jobsgGroup3 = 2
        while jobsgGroup3 < processQuantity:
            jobsgGroup1 += 2
            jobsgGroup2 += 2
            jobsgGroup3 += 2
            groupsThread[jobsgGroup1]>>[groupsThread[jobsgGroup2],groupsThread[jobsgGroup3]]
        groupsThread[jobsgGroup3]>>groupsThread[jobsgGroup3+1]
    return Thread

#[Close Thask thread constructor Lite]
def closeTaskGroup(Threads_close,dag,jobs_close):
    count_close = 0
    while count_close < len(jobs_close):
        close_task = PythonOperator(
                                    task_id=f'{jobs_close[count_close][0]}',
                                    python_callable=executing_job,
                                    op_kwargs={'job_identifier': f'{jobs_close[count_close][1]}'},
                                    dag=dag,
                                    )
        count_close += 1
        Threads_close.append(close_task)
    return Threads_close

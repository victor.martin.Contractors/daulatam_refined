from airflow.utils.task_group import TaskGroup
from src.model.Taskgroup import *
from src.DAO.StreamsetsDAO import *

def activation_key():
    activation_key = get_activation_key()
    return activation_key

#[Thask thread controller]
def buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag):
    Threads = TaskGroup(group_id='Taks')
    Thread_task = []
    #Creating group taks by thread
    while Thread_cicle < ThreadQuantity:
        Thread = createTaskGroup(jobs,Thread_cicle,dag,Threads)
        Thread_task.append(Thread)
        Thread_cicle += 1
    return Threads

#[Thask close thread controller]
def CloseTask(dag,jobs_close):
    Threads_close=[]
    with TaskGroup(group_id='Thread_close') as Thread_close:
        Threads_close = closeTaskGroup(Threads_close,dag,jobs_close)
    Threads_close[0]>>Threads_close[1]
    return Thread_close

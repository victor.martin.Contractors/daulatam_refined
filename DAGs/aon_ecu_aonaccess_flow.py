from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_ecu_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_ecu_aonaccess_flow',schedule_interval='0 3 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Ecuador']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
    #Preparing environment [only valid in snowflake migration]
    #truncate = executingjob(dag,'0380ead0-9547-4e50-87f5-14095279c40d:aon.com','snowflaketruncate')
    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
    #Finishing process
    #Close = CloseTask(dag,jobs_close)  
#Managing dependencies 
connect>>Taks

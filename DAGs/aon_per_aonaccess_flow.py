from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_per_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_per_aonaccess_flow',schedule_interval='0 3 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Peru']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
    #Preparing environment [only valid in snowflake migration]
    truncate = executingjob(dag,'4afc4f8a-c307-4d77-88bc-72309cd9176f:aon.com','snowflaketruncate')
    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
    #Finishing process
    #Close = CloseTask(dag,jobs_close)  
#Managing dependencies 
truncate>>connect>>Taks

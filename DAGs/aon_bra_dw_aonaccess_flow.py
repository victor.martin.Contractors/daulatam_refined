from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_bra_dw_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_bra_dw_aonaccess_flow',schedule_interval='0 11 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Brasil','Datawarehouse']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
            
    #fact_receita_operacao_clean = executingjob(dag,'80f61605-8fff-4817-9e34-b1a01620d445:aon.com','fact_receita_operacao_clean')
    #fact_receita_operacao_raw = executingjob(dag,'2437830e-4c4a-47c8-975d-0276f3aca7b8:aon.com','fact_receita_operacao_raw')
    #fact_receita_operacao_discovery = executingjob(dag,'4fa75edb-73a0-4af8-adb5-f5fbb625c415:aon.com','fact_receita_operacao_discovery')

    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
    #Finishing process
    #Close = CloseTask(dag,jobs_close) 
#Managing dependencies  
#connect>>fact_receita_operacao_clean>>fact_receita_operacao_raw>>fact_receita_operacao_discovery
connect>>Taks
from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_qa_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_qa_aonaccess_flow',schedule_interval='30 20 * * *',default_args=default_args,catchup=False,tags=['DnAUnitedTest','Testing']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
    #Preparing environment [only valid in snowflake migration]

    ARGENTINA = executingjob(dag,'e31a3728-26d1-4ce9-9178-1ab29a6f7fb7:aon.com','ARGENTINA')
    BRAZIL = executingjob(dag,'a0b7c3f5-902a-4e4a-86cb-3146416d88c5:aon.com','BRAZIL')
    COLOMBIA = executingjob(dag,'7362241e-57e7-477f-a5e8-64eae9298f54:aon.com','COLOMBIA')
    CHILE = executingjob(dag,'51c5cdb1-b6f3-459e-a5fb-dc12c72a3069:aon.com','CHILE')
    ECUADOR = executingjob(dag,'0380ead0-9547-4e50-87f5-14095279c40d:aon.com','ECUADOR')
    MEXICO = executingjob(dag,'5aba0668-c813-43ad-a9dd-613217b977c3:aon.com','MEXICO')
    PERU = executingjob(dag,'4afc4f8a-c307-4d77-88bc-72309cd9176f:aon.com','PERU')
    PUERTO_RICO = executingjob(dag,'f63fe099-66c5-4b0d-a83b-b80562ff9037:aon.com','PUERTO_RICO')
    VENEZUELA = executingjob(dag,'b6a39a07-0abe-437b-afc0-ca2c672d6ff0:aon.com','VENEZUELA')
    DW_BRAZIL = executingjob(dag,'ca5aba02-7234-4d78-861a-958e42e789de:aon.com','DW_BRAZIL')
    SIGB = executingjob(dag,'335a1037-bb73-4a53-bccc-fe3e40b00643:aon.com','SIGB')

    
    ARGENTINA>>CHILE>>PERU>>DW_BRAZIL
    BRAZIL>>ECUADOR>>PUERTO_RICO>>SIGB
    COLOMBIA>>MEXICO>>VENEZUELA
#Managing dependencies 
connect>>[ARGENTINA,BRAZIL,COLOMBIA]

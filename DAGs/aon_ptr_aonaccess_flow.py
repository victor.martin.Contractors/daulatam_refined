from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_ptr_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_ptr_aonaccess_flow',schedule_interval='0 5 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Puerto Rico']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
    #Preparing environment [only valid in snowflake migration]
    #truncate = executingjob(dag,'f63fe099-66c5-4b0d-a83b-b80562ff9037:aon.com','snowflaketruncate')
    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
    #Finishing process
    #Close = CloseTask(dag,jobs_close)  
#Managing dependencies 
connect>>Taks

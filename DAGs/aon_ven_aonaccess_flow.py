from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_ven_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_ven_aonaccess_flow',schedule_interval='0 22 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Venezuela']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
    #Preparing environment [only valid in snowflake migration]
    sg_siniestrocuentacorriente_clean=executingjob(dag,'7a18cb5a-14dd-4103-99ee-0026b509c8ca:aon.com','sg_siniestrocuentacorriente_clean')
    sg_siniestrocuentacorriente_raw=executingjob(dag,'80d22cfb-beb6-49c5-994c-c3025d023a3c:aon.com','sg_siniestrocuentacorriente_raw')
    sg_siniestrocuentacorriente_discovery=executingjob(dag,'eba302c8-57d7-4fba-bfd2-bd125e373b76:aon.com','sg_siniestrocuentacorriente_discovery')
   
    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
    #Finishing process
    #Close = CloseTask(dag,jobs_close)
    #Managing dependencies 
    sg_siniestrocuentacorriente_clean>>sg_siniestrocuentacorriente_raw>>sg_siniestrocuentacorriente_discovery
connect>>[Taks,sg_siniestrocuentacorriente_clean]
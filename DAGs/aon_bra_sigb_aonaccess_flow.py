from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_bra_sigb_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_bra_sigb_aonaccess_flow',schedule_interval='30 21 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Brasil']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
    
    smc_ad_movimentacao_clean = executingjob(dag,'d1477fe8-5cd2-406d-9b5a-a8ddb288a373:aon.com','smc_ad_movimentacao_clean')
    smc_ad_movimentacao_raw = executingjob(dag,'b917476e-5671-4edb-b32a-4ef776587a4e:aon.com','smc_ad_movimentacao_raw')
    smc_ad_movimentacao_discovery = executingjob(dag,'bef06fb3-8a00-44f9-9439-20f9be3852ad:aon.com','smc_ad_movimentacao_discovery')
    
    smc_usuario_raw = executingjob(dag,'bd069c85-4476-44c2-ab16-96ffa434fc2f:aon.com','smc_usuario_raw')
    smc_usuario_discovery = executingjob(dag,'93c34709-b88b-45af-a4d6-2f87aab303f4:aon.com','smc_usuario_discovery')
    smc_usuario_refined = executingjob(dag,'49b99375-9f42-4d13-9c13-5f129f8cfb4b:aon.com','smc_usuario_refined')

    ad_pessoa_raw = executingjob(dag,'1198c725-2d0e-40fc-a41a-df9212a6c8f0:aon.com','ad_pessoa_raw')
    ad_pessoa_discovery = executingjob(dag,'19bb513b-09ee-4908-ae96-bcfa5cb0245a:aon.com','ad_pessoa_discovery')
    ad_pessoa_refined = executingjob(dag,'090bd831-d11b-4789-a89d-a99318c803e6:aon.com','ad_pessoa_refined')

    ad_beneficiario_raw = executingjob(dag,'c4918444-c6c6-49fe-b92c-67f471d46079:aon.com','ad_beneficiario_raw')
    ad_beneficiario_discovery = executingjob(dag,'7fab43ff-8750-4f3c-95f8-4f2442876443:aon.com','ad_beneficiario_discovery')
    ad_beneficiario_refined = executingjob(dag,'0101ef4f-94d8-4301-9841-f5d47d82198e:aon.com','ad_beneficiario_refined')

    smc_ad_movimentacao_historico_raw = executingjob(dag,'815019b4-9a14-4e0b-a45d-ef7fefc228ed:aon.com','smc_ad_movimentacao_historico_raw')
    smc_ad_movimentacao_historico_discovery = executingjob(dag,'aa6cf0fe-caa9-4a2c-a525-ef9060cef441:aon.com','smc_ad_movimentacao_historico_discovery')
    smc_ad_movimentacao_historico_refined = executingjob(dag,'1823d753-8290-4d06-b855-c492d4b49f47:aon.com','smc_ad_movimentacao_historico_refined')

    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
    #Finishing process
    #Close = CloseTask(dag,jobs_close) 
#Managing dependencies  

smc_ad_movimentacao_historico_raw>>smc_ad_movimentacao_historico_discovery>>smc_ad_movimentacao_historico_refined
smc_ad_movimentacao_clean>>smc_ad_movimentacao_raw>>smc_ad_movimentacao_discovery
ad_pessoa_raw>>ad_pessoa_discovery>>ad_pessoa_refined
smc_usuario_raw>>smc_usuario_discovery>>smc_usuario_refined>>ad_pessoa_raw
ad_beneficiario_raw>>ad_beneficiario_discovery>>ad_beneficiario_refined
smc_usuario_raw>>ad_beneficiario_raw
smc_ad_movimentacao_clean>>smc_ad_movimentacao_historico_raw
##Dependences
connect>>[Taks,smc_ad_movimentacao_clean,smc_usuario_raw]
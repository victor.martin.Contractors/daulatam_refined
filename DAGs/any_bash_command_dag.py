from airflow.models import DAG
from params.aon_params import *
from src.DAO.StreamsetsDAO import *
from src.model.SingletonControlHub import *
#Importing configuration File
from params.any_bash_command_dag_params import *
from airflow.operators.python_operator import PythonOperator
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()

connection_control_hub = None


def control_hub_first():
    global connection_control_hub
    streamsetsUser = get_streamsets_user()
    streamsetsPassword = get_streamsets_password()
    logging.info('first Control hub Loggin')
    if connection_control_hub is None:
        logging.info('first first')
        connection_control_hub = ControlHub(server_url='https://cloud.streamsets.com',
                                username=streamsetsUser,
                                password=streamsetsPassword)

    return connection_control_hub





#DAG
with DAG('any_bash_command_dag',schedule_interval='@once',default_args=default_args,catchup=False,tags=['DnAUnited','any_bash_command_dag']) as dag:
    #First connection environment comprobation
   # control_hub = get_control_hub_conn()
    #hub =gettingcontrolhub(dag)

    #smc_usuario_raw = executingjobfromlistv2(dag,hub,'bd069c85-4476-44c2-ab16-96ffa434fc2f:aon.com','smc_usuario_raw')
    #t1 = PythonOperator(
    #                    task_id='control_hub_first',
    #                    provide_context=True,
    #                    python_callable=control_hub_first,
    #                    dag=dag,
    #)

    #t2 = PythonOperator(
    #                    task_id='control_hub_first2',
    #                    provide_context=True,
    #                    python_callable=control_hub_first,
    #                    dag=dag,
    #)

    #connect = gettingjobsStreamsets(dag)
    #connect = SingletonControlHub.gettingjobsStreamsets(dag)
    
    #Preparing environment [only valid in snowflake migration]
    #building Streamsets task threads
    #Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
        
    smc_usuario_raw = executingjobfromlist(dag,'f32dd661-150c-4920-9721-21394c9a3ab9:aon.com','smc_usuario_raw')
    #smc_usuario_discovery = executingjobfromlist(dag,'660287c7-1f26-4c35-a12d-c7a6590946e1:aon.com','smc_usuario_discovery')
    #smc_usuario_refined = executingjob(dag,'49b99375-9f42-4d13-9c13-5f129f8cfb4b:aon.com','smc_usuario_refined')

    #Finishing process
    #Close = CloseTask(dag,jobs_close)
#Managing dependencies 
smc_usuario_raw
#finished
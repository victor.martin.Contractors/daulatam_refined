from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
#from params.aon_bra_dw_aonaccess_params_step2 import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_bra_dw_aonaccess_flow_step2',schedule_interval='0 9 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Brasil','Datawarehouse']) as dag:
    #First connection environment comprobation
    #connect = connectToStreamsets(dag)
    #cleaning raw
    depurator = executingjob(dag,'ab35a61d-c137-49fb-aa5b-a3e5bcec3352:aon.com','clean_raw')
    #Preparing environment [only valid in snowflake migration]
    snowraw = executingjob(dag,'3bce68ee-3965-41e1-8634-3a138bd286fe:aon.com','temp_vw_receita_powerbi_snowflake_Raw')
    snowdiscovery = executingjob(dag,'2ec29339-9412-4aaf-93f7-501ebf2fe986:aon.com','temp_vw_receita_powerbi_snowflake_Disc')
    #building Streamsets task threads
    #Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
#Managing dependencies
depurator>>snowraw>>snowdiscovery
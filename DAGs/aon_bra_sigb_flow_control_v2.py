from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_qa_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#Building daily Dag 
with DAG('aon_bra_sigb_flow_control_v2',schedule_interval='@once',default_args=default_args,catchup=False,tags=['DnAUnited','Brasil']) as dag:
    #First connection
    connect = connectToStreamsets(dag)
    #Preparing environment [only valid in snowflake migration]
    SG_CUENTACORRIENTEPREMIO1 = executingjob(dag,'c6d843ad-9022-4e57-8c1d-39894f947f3b:aon.com','SG_CUENTACORRIENTEPREMIO1')
    SG_CUENTACORRIENTEPREMIO2 = executingjob(dag,'26292c5c-7ecf-49b8-9254-2cf126692af5:aon.com','SG_CUENTACORRIENTEPREMIO2')
    SG_CUENTACORRIENTEPREMIO3 = executingjob(dag,'22cfd2f5-38ad-4405-8659-4a70680c64d6:aon.com','SG_CUENTACORRIENTEPREMIO3')
    SG_CUENTACORRIENTEPREMIO4 = executingjob(dag,'cb54169d-52e7-41d8-9803-3fb34d104a4d:aon.com','SG_CUENTACORRIENTEPREMIO4')
    SG_CUENTACORRIENTEPREMIO5 = executingjob(dag,'829530b1-ede7-4b87-840b-d726206a2d0a:aon.com','SG_CUENTACORRIENTEPREMIO5')
    SG_CUENTACORRIENTEPREMIO6 = executingjob(dag,'4e1decfb-e55f-47c5-9be5-a3dada3f319e:aon.com','SG_CUENTACORRIENTEPREMIO6')
    SG_CUENTACORRIENTEPREMIO7 = executingjob(dag,'85a78261-e081-41d3-b8d5-3803bf01293e:aon.com','SG_CUENTACORRIENTEPREMIO7')

    SG_CUENTACORRIENTEPREMIO1>>SG_CUENTACORRIENTEPREMIO4>>SG_CUENTACORRIENTEPREMIO7
    SG_CUENTACORRIENTEPREMIO2>>SG_CUENTACORRIENTEPREMIO5
    SG_CUENTACORRIENTEPREMIO3>>SG_CUENTACORRIENTEPREMIO6
#Managing dependencies 
connect>>[SG_CUENTACORRIENTEPREMIO1,SG_CUENTACORRIENTEPREMIO2,SG_CUENTACORRIENTEPREMIO3]

from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_arg_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_arg_aonaccess_flow',schedule_interval='0 21 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Argentina']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)

    tg_entedomicilio_clean = executingjob(dag,'c0534b36-5dd8-44f3-918d-9e3992e6d984:aon.com','tg_entedomicilio_clean')
    tg_entedomicilio_raw = executingjob(dag,'e9d0bd20-bf06-4ace-841c-ace939d3d381:aon.com','tg_entedomicilio_raw')
    tg_entedomicilio_discovery = executingjob(dag,'dcd19a94-0757-46ab-8b4b-103d5a531a0b:aon.com','tg_entedomicilio_discovery')

    sg_siniestrocausa_clean = executingjob(dag,'d01985d5-110e-4bfa-b727-fc0d94a6b19f:aon.com','sg_siniestrocausa_clean')
    sg_siniestrocausa_raw = executingjob(dag,'b78d273b-20e1-41fa-94fa-fc5f8b5dcbbf:aon.com','sg_siniestrocausa_raw')
    sg_siniestrocausa_discovery = executingjob(dag,'31d3c1e9-3e34-437e-9d87-49394b5acca2:aon.com','sg_siniestrocausa_discovery')

    sg_honorarioitem_clean = executingjob(dag,'43c9de2f-40e5-4421-879b-a7155409aaa4:aon.com','sg_honorarioitem_clean')
    sg_honorarioitem_raw = executingjob(dag,'9a0465fa-91e5-4ac6-9c53-bdcc577973e2:aon.com','sg_honorarioitem_raw')
    sg_honorarioitem_discovery = executingjob(dag,'9bcf8726-9e76-4d4a-9587-dc1ba5c31bdc:aon.com','sg_honorarioitem_discovery')

    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)

    sg_honorarioitem_clean>>sg_honorarioitem_raw>>sg_honorarioitem_discovery
    tg_entedomicilio_clean>>tg_entedomicilio_raw>>[tg_entedomicilio_discovery,sg_honorarioitem_clean]
    sg_siniestrocausa_clean>>sg_siniestrocausa_raw>>[sg_siniestrocausa_discovery,tg_entedomicilio_clean]

    #Finishing process
    #Close = CloseTask(dag,jobs_close)

#Managing dependencies 
connect>>[sg_siniestrocausa_clean,Taks]

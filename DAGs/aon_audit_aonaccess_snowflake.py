from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_audit_aonaccess_snowflake_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_audit_aonaccess_snowflake',schedule_interval='0 10 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Latam']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
    #Preparing environment [only valid in snowflake migration]
    Preparation = executingjob(dag,'ae2238ea-b668-4af2-af3a-9444494cb799:aon.com','Preparation')
    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
    #Finishing process
    ColombiaPnR = executingjob(dag,'f3101276-19e4-49f8-ad27-f5540a80b3c0:aon.com','ColombiaPnR')
    DetectingNullData = executingjob(dag,'5e2d2648-aa44-4dbb-9606-4ad44119b317:aon.com','DetectingNullData')
    DetectingDuplicateData = executingjob(dag,'13e7000e-d0a2-4079-862a-3b5d2495d7b3:aon.com','DetectingDuplicateData')
    DetectingNullData_stage2 = executingjob(dag,'bf5d34a7-d411-460a-be40-08e7ab3d6613:aon.com','DetectingNullData_stage2')
    DetectingDuplicateData_stage2 = executingjob(dag,'33c27093-4976-4fd1-a8cc-013a74b7ac9e:aon.com','DetectingDuplicateData_stage2')
    DetectingNullData_stage3 = executingjob(dag,'0afc11e0-2819-41aa-b006-42f01d675bb4:aon.com','DetectingNullData_stage3')
    DetectingDuplicateData_stage3 = executingjob(dag,'bb2abe8a-307b-46bf-94c3-df85cffcb242:aon.com','DetectingDuplicateData_stage3')

    Close = executingjob(dag,'265a58e1-7599-46a9-92ad-43aeb3ca1f20:aon.com','Close')
    
    Preparation>>DetectingNullData
    Preparation>>DetectingDuplicateData
    Preparation>>DetectingNullData_stage2
    Preparation>>DetectingDuplicateData_stage2
    Preparation>>DetectingNullData_stage3
    Preparation>>DetectingDuplicateData_stage3
    DetectingNullData_stage3>>Close
    DetectingDuplicateData_stage3>>Close
    DetectingNullData_stage2>>Close
    DetectingDuplicateData_stage2>>Close
    DetectingNullData>>Close
    DetectingDuplicateData>>Close
    #Managing dependencies 
connect>>Preparation>>[Taks,ColombiaPnR]>>Close
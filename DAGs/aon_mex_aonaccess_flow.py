from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_mex_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_mex_aonaccess_flow',schedule_interval='0 23 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Mexico']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
    #Preparing environment [only valid in snowflake migration]
    #truncate = executingjob(dag,'5aba0668-c813-43ad-a9dd-613217b977c3:aon.com','snowflaketruncate')

    tg_entedomicilio_clean = executingjob(dag,'43295df4-c7b4-4eff-8584-d95453cbace8:aon.com','tg_entedomicilio_clean')
    tg_entedomicilio_raw = executingjob(dag,'936eb8f5-830d-4677-bed6-5436bacb50f3:aon.com','tg_entedomicilio_raw')
    tg_entedomicilio_discovery = executingjob(dag,'5f3f9c01-6396-4197-aba0-4b0de08309ed:aon.com','tg_entedomicilio_discovery')

    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
    tg_entedomicilio_clean>>tg_entedomicilio_raw>>tg_entedomicilio_discovery
    #Finishing process
    #Close = CloseTask(dag,jobs_close)  
#Managing dependencies 
connect>>[Taks,tg_entedomicilio_clean]
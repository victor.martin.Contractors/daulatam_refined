from airflow.models import DAG
from params.aon_params import *
#Importing configuration File
from params.aon_bra_aonaccess_params import *
#Getting Secret key in secrets Manager
os.environ["STREAMSETS_SDK_ACTIVATION_KEY"] = activation_key()
#DAG
with DAG('aon_bra_aonaccess_flow',schedule_interval='0 23 * * *',default_args=default_args,catchup=False,tags=['DnAUnited','Brazil']) as dag:
    #First connection environment comprobation
    connect = connectToStreamsets(dag)
    #Preparing environment [only valid in snowflake migration]
    #truncate = executingjob(dag,'a0b7c3f5-902a-4e4a-86cb-3146416d88c5:aon.com','snowflaketruncate')
    #building Streamsets task threads
    Taks = buildTaskThreads(ThreadQuantity,jobs,Thread_cicle,dag)
    #Finishing process
    #Close = CloseTask(dag,jobs_close)
#Managing dependencies  
connect>>Taks
